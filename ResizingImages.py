from PIL import Image
import shutil
import os
import random
from resizeimage import resizeimage

FILE_DIR = 'path'
RESIZED_PATH = 'resizedpath'

"""
Input : single image path
Output : width of height of picture
"""
def FindSize(img_path):
    with Image.open(img_path) as img:
        width, height = img.size

    bigger_side, smaller_size = lambda : width,height if width>=height else height,width
    size_ratio = bigger_side / smaller_size
    return width, height, size_ratio

"""
Resize all images and saving in a new directory
"""
def ResizeAllPictures():
    dirs = os.listdir(FILE_DIR)
    for img in dirs:
        full_path = FILE_DIR + img
        if os.path.isfile(full_path):
            width, height, ratio = FindSize(full_path)
            im = Image.open(full_path)
            if width >= height:
                imResize = im.resize((255, width * (1 / ratio)), Image.ANTIALIAS)
            else:
                imResize = im.resize((width * ratio, 255), Image.ANTIALIAS)

            imResize.save(RESIZED_PATH + img, 'JPEG')


def main():
    ResizeAllPictures()


if __name__ == '__main__':
    main()
