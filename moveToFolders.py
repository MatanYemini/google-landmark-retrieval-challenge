import shutil
import os


def main():
    basedir = os.path.curdir
    for filename in filter(lambda f: os.path.isfile(f), os.listdir(basedir)):
        if not filename.endswith('jpg'):
            continue

        print('Copying file', filename)
        landmark_id, new_filename = filename.split('_')

        # Initial new folder name
        new_folder = os.path.join(os.path.curdir, landmark_id)

        if not os.path.exists(new_folder):
            print(new_folder, "not found, creating folder")
            os.mkdir(new_folder)

        new_destination_file = os.path.join(new_folder, new_filename)
        if not os.path.exists(new_destination_file):
            shutil.copy(filename, new_destination_file)
        else:
            print('Files %s already exists no need to copy' % new_filename)
            continue


if __name__ == '__main__':
    main()