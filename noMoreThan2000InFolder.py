import shutil
import os
import random

def deleteFilesRandom(path):
    imagesInDirList = os.listdir(path)
    numberOfImagesInDir = len(imagesInDirList)

    while(numberOfImagesInDir > 2000):
        randomImagIndex = random.randint(1, numberOfImagesInDir - 1)
        print("Delelting image: " + imagesInDirList[randomImagIndex])
        os.remove(os.path.join(path, imagesInDirList[randomImagIndex]))
        
        imagesInDirList = os.listdir(path)
        numberOfImagesInDir = len(imagesInDirList)

def main():
    rootdir = os.path.curdir
    counter = 1
    for dirs in os.listdir(rootdir):
        if(os.path.isdir(dirs)): 
            imagesInDir = os.listdir(os.path.join(rootdir, dirs))
            numberOfImagesInDir = len(imagesInDir)
            print(str(counter) + ". " +"There are " + str(numberOfImagesInDir) + " images in folder: " + dirs)
            counter += 1
            
            #deleteFilesRandom(dirs)
                
if __name__ == '__main__':
    main()